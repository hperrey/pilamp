#!/usr/bin/env python3

import pigpio
import socket
import sys
import wavePWM
import threading
import time
import math
import logging
import os
import argparse

class LedColor:
    """Class to hold color values as fractions 0..1"""
    def __init__(self, red = 0, green = 0, blue = 0):
        self.value = {}
        self.value['red'] = red
        self.value['green'] = green
        self.value['blue'] = blue
    def set(self, red = 0, green = 0, blue = 0):
        self.value['red'] = red
        self.value['green'] = green
        self.value['blue'] = blue
    def get(self):
        return self.value['red'], self.value['green'], self.value['blue']
    def __eq__(self, color):
        """Tests against another color for equality."""
        if (math.isclose(self.value['red'], color.value['red']) and
            math.isclose(self.value['green'], color.value['green']) and
            math.isclose(self.value['blue'], color.value['blue'])):
            return True
        else:
            return False

# dictionary providing a mapping of known colors
known_colors = {
    "red"   : LedColor(red = 1),
    "green" : LedColor(green = 1),
    "blue"  : LedColor(blue = 1),
    "purple": LedColor(0.7, 0, 0.99),
    "off"   : LedColor()
}

def identify_color(color):
    """ searches known colors for a match and returns label for the given LedColor """
    for key in known_colors:
        if known_colors[key] == color:
            return key
    return "unknown"

class Client(threading.Thread):
    """class for handling client connections"""
    def __init__(self, socket, address, color):
        super().__init__()
        self.sock = socket
        self.addr = address
        self.color = color
        self.log = logging.getLogger(__name__)
        self.start()

    def run(self):
        self.sock.send(b'READY\n')
        while 1:
            # TODO: implement timeout
            data = self.sock.recv(1024)
            if not data:
                break
            self.log.debug('Client sent: {}'.format(data.decode()))
            # identify color from string
            if "get_color" in data.decode():
                self.sock.send(b'OK\n')
                self.sock.send('{} {} {} {}\n'.format(identify_color(self.color), *self.color.get()).encode())
            if "set_rgb" in data.decode():
                parsed_args = list(filter(None, data.decode().split(" "))) # split string, remove empty entries
                try:
                    self.color.set(float(parsed_args[1]), float(parsed_args[2]), float(parsed_args[3]))
                    self.sock.send(b'OK\n')
                except (ValueError, IndexError) as e:
                    self.log.debug('Caught {} exception trying to parse set_rgb cmd'.format(str(e)))
                    self.sock.send(b'ERROR parsing arguments to command: set_rgb [R] [G] [B]\n')
            # search for color names and set color if found at beginning of data
            for c in known_colors:
                if data.decode().startswith(c):
                    self.color.set(*known_colors[c].get())
                    self.sock.send(b'OK\n')
                    break
            self.sock.send(b'READY\n')
        self.log.info('Client closed connection')
        self.sock.close()

class ButtonToggle(threading.Thread):
    """Class handling button presses via GPIO pins to toggle light status."""
    def __init__(self, input_pin, color):
        super().__init__() # call Thread base class init
        self.input_pin = input_pin
        self._stop_event = threading.Event() # to allow controlled stop
        self.color = color
        self.log = logging.getLogger(__name__)
        self.__pi = pigpio.pi() # init GPIO connection
        if not self.__pi.connected:
            self.log.error("ERROR: Cannot establish pigpiod connection for ButtonToggle class.")
            exit()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):
        """Method monitoring GPIO pin state and running as long as thread is active."""

        # configure input_pin as INPUT and LOW (if connected to 3.3V)
        self.__pi.set_mode(self.input_pin, pigpio.INPUT)
        self.__pi.set_pull_up_down(self.input_pin, pigpio.PUD_DOWN)

        while not self.stopped():
            inputValue = self.__pi.read(self.input_pin)
            if (inputValue == False):
                self.log.debug('Button pressed')
                if self.color == LedColor(0.7, 0, 0.99):
                    # already purple, set off
                    self.color.set(0,0,0)
                else:
                    self.color.set(0.7, 0, 0.99)
                # wait up to 1s for the button to be released:
                self.__pi.wait_for_edge(self.input_pin, pigpio.RISING_EDGE, 1.0)
            time.sleep(0.2)
        self.__pi.stop() # cleanup GPIO resources


class LedLight():
    """Class for operating an RGB led via PWM on GPIO pins. To be used in the form
    with LedLight(...) as led:
        ...
    """
    def __init__(self, red_pin, green_pin, blue_pin):
        """init method setting up the PWM"""
        self.pins = {}
        self.pins['red'] = red_pin
        self.pins['green'] = green_pin
        self.pins['blue'] = blue_pin
        self.log = logging.getLogger(__name__)

    def __enter__(self):
        """run when entering 'with' statement"""
        freq = 10000
        self.__pi = pigpio.pi()
        if not self.__pi.connected:
            self.log.error("Error accessing pigpio.pi()")
            sys.exit(0)
        self.__pwm = wavePWM.PWM(self.__pi) # Use default frequency
        if freq % 400:
            self.__pwm.set_frequency(freq)
        else:
            self.__pwm.set_cycle_time(1000000.0/freq)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """closing method"""
        self.__pwm.cancel()
        self.__pi.stop()

    def fade_colors(self, start_color, stop_color, steps = 100, wait = 0.01):
        for i in range(steps-1):
            for color, pin in self.pins.items():
                # transitional color
                c = start_color.value[color]+((stop_color.value[color]-start_color.value[color])* i/steps)
                self.__pwm.set_pulse_length_in_fraction(pin, c)
            self.__pwm.update() # Apply all the changes.
            time.sleep(wait)

    def fade_to_color(self, stop_color, steps = 100, wait = 0.01):
        self.fade_colors(self.get_color(), stop_color, steps, wait)

    def fade_to_off(self, steps = 100, wait = 0.01):
        self.fade_colors(self.get_color(), LedColor(0,0,0), steps, wait)

    def get_color(self):
        colors = {}
        for color, pin in self.pins.items():
            _a, _s, colors[color] = self.__pwm.get_GPIO_settings(pin)
        l = LedColor()
        l.value = colors
        return l

    def set_color(self, color):
        for name, pin in self.pins.items():
            # colors can't take values greater 1 -- max out at 0.999
            c = color.value[name] if color.value[name] < 1 else 0.999
            self.__pwm.set_pulse_length_in_fraction(pin, c)
        self.__pwm.update() # Apply all the changes.
        time.sleep(0.01)

    def off(self):
        # switch all off
        for color, pin in self.pins.items():
            self.__pwm.set_pulse_length_in_micros(pin, 0)
        self.__pwm.update()

    def color_test(self):
        for color, pin in self.pins.items():
            self.__pwm.set_pulse_start_in_fraction(pin, 0)
            # fade up
            for percent in range(100):
                self.__pwm.set_pulse_length_in_fraction(pin, percent/100.)
                self.__pwm.update() # Apply all the changes.
                time.sleep(0.01)
            # and off again
            self.__pwm.set_pulse_length_in_micros(pin, 0)


class Dimmer(threading.Thread):
    """Threaded dimmer class controlling an RGB led light."""

    def __init__(self, led_light, led_color, dopulse):
        super().__init__()
        self._stop_event = threading.Event()
        self.light = led_light
        self.color = led_color
        self.dopulse = dopulse

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):
        """Overloaded Thread.run, changes state of led to pulse."""
        while not self.stopped():
            if self.dopulse:
                self.light.fade_to_color(self.color)
                self.light.fade_to_off()
            else:
                self.light.set_color(self.color)
                time.sleep(0.1)

def init_socket():
    HOST = ''	# Symbolic name meaning all available interfaces
    PORT = 12345	# Arbitrary non-privileged port
    log = logging.getLogger(__name__)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    log.debug('Socket created')

    #Bind socket to local host and port
    try:
        s.bind((HOST, PORT))
    except OSError as e:
        log.error('Bind failed. Error Code : ' + str(e.errno) + ' Message ' + e.strerror)
        sys.exit()

    log.debug('Socket bind complete')

    #Start listening on socket
    s.listen(10)
    log.info('Socket now listening on port {}'.format(PORT))
    return s

def main():
    # setup logging
    logging.basicConfig()
    log = logging.getLogger(__name__)
    #log.addHandler(logging.FileHandler('light.log'))

    # command line argument parsing
    argv = sys.argv
    progName = os.path.basename(argv.pop(0))
    parser = argparse.ArgumentParser(
        prog=progName,
        description=
        "RaspberryPi Lamp control server"
    )
    parser.add_argument(
        "-l",
        "--log-level",
        default="warning",
        help=
        "Sets the verbosity of log messages where LEVEL is either debug, info, warning or error",
        metavar="LEVEL")
    parser.add_argument('-r', '--red',
                        help="GPIO pin number where the red component of the LED is connected to",
                        default='22',
                        type=int,
                        metavar='PIN'
    )
    parser.add_argument('-g', '--green',
                        help="GPIO pin number where the green component of the LED is connected to",
                        default='23',
                        type=int,
                        metavar='PIN'
    )
    parser.add_argument('-b', '--blue',
                        help="GPIO pin number where the blue component of the LED is connected to",
                        default='24',
                        type=int,
                        metavar='PIN'
    )
    parser.add_argument('--button',
                        help="GPIO pin number where the button is connected to",
                        default='18',
                        type=int,
                        metavar='PIN'
    )
    parser.add_argument("--no-button", help="Disables interaction via button press",
                        action="store_true")
    parser.add_argument("--no-pulsing", help="Disables automatic pulsing of color from and to off state",
                        action="store_true", default=False)

    # parse the arguments
    args = parser.parse_args(argv)
    # set the logging level
    numeric_level = getattr(logging, "WARNING",
                            None)  # set default
    if args.log_level:
        # Convert log level to upper case to allow the user to specify --log-level=DEBUG or --log-level=debug
        numeric_level = getattr(logging, args.log_level.upper(), None)
        if not isinstance(numeric_level, int):
            log.error('Invalid log level: %s' % args.log_level)
            sys.exit(2)
    log.setLevel(numeric_level)

    log.info('Starting up...')

    with LedLight(red_pin = args.red, green_pin = args.green, blue_pin = args.blue) as light:

        s = init_socket()
        color = LedColor(0, 0, 0) # corresponds to 'off'
        d = Dimmer(light, color, dopulse=(not args.no_pulsing))

        if not args.no_button:
            b = ButtonToggle(input_pin = args.button, color = color)

        try:
            d.start()
            if not args.no_button:
                b.start()
            # now keep talking with the client
            while True:
                # wait to accept a connection - blocking call
                clientsocket, address = s.accept()
                log.info( 'Connected with ' + address[0] + ':' + str(address[1]))
                Client(clientsocket, address, color)

        except KeyboardInterrupt:
            pass

        log.info("\ntidying up")
        s.close()
        if not args.no_button:
            b.stop()
            b.join()
        d.stop()
        d.join()
        light.off()

if __name__ == "__main__":
    main()

