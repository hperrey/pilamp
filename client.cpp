#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
  int sockfd = 0;
  char recvBuff[1024];
  struct sockaddr_in serv_addr;
  fd_set fdset;
  struct timeval tv;


  if(argc != 2)
    {
      printf("\n Usage: %s <ip of server> \n",argv[0]);
      return 1;
    }

  memset(recvBuff, '0',sizeof(recvBuff));
  if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
      printf("\n Error : Could not create socket \n");
      return 1;
    }

  memset(&serv_addr, '0', sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(12345);

  if(inet_pton(AF_INET, argv[1], &serv_addr.sin_addr)<=0)
    {
      printf("\n inet_pton error occured\n");
      return 1;
    }
  // put into non-blocking mode
  fcntl(sockfd, F_SETFL, O_NONBLOCK);

  connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));

  FD_ZERO(&fdset);
  FD_SET(sockfd, &fdset);
  tv.tv_sec = 10;             /* 10 second timeout */
  tv.tv_usec = 0;

  if (select(sockfd + 1, NULL, &fdset, NULL, &tv) == 1){
    int so_error;
    socklen_t len = sizeof so_error;

    getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &so_error, &len);

    if (so_error == 0) {
      printf("Connection is open\n");
      char sendBuff[] = "purple\r\n";
      write(sockfd, sendBuff, strlen(sendBuff));

      printf("Send string, closing connection\n");

    } else {
      printf("Error occurred!\n");
    }
  } else {
    printf("Timeout connecting!\n");
  }


  //snprintf(sendBuff, siAzeof(sendBuff), "%.24s\r\n", ctime(&ticks));
  close(sockfd);
  return 0;
}
