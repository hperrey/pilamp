#!/usr/bin/env python3

import socket
import time

def main():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('localhost', 12345) # default port for pilamp server
    # set timeout
    sock.settimeout(1) # tcp connection will take time
    print('Connecting')
    # establish connection
    sock.connect(server_address)
    time.sleep(0.1)
    try:
        buffer = sock.recv(1024).decode()
    except socket.error as e:
        print('Error: %r' % e)
    print('Setting color')
    msg = 'set_rgb 0 1 0'
    sock.sendall(msg.encode())
    time.sleep(.1)
    try:
        buffer = sock.recv(1024).decode()
    except socket.error as e:
        print('Error: %r' % e)
    if buffer.startswith('OK'):
        print(f"Got response: 'OK'")
    print('Closing connection')
    sock.close()

if __name__ == '__main__':
    main()
