# Remote RGB Light Controller for Raspberry Pi
Allows selecting a color on a high-power RGB LED via network socket. The LED is
controlled via pulse-width modulation (PWM) on the GPIO pins of the Raspberry Pi
and dimmed to create a pulsing effect.

## Components
    [Triple Output High Power RGB LED](https://www.sparkfun.com/products/8718)
    [PicoBuck LED Driver](https://www.sparkfun.com/products/13705)

## Wiring
    Connect the PicoBuck analog inputs to GPIO pins of the Raspberry (e.g.
    22--24). Connect an optional button to 3.3V and an GPIO pin (e.g. 18).

## Setup
    - install Python3 and [pigpio](http://abyz.me.uk/rpi/pigpio/pigpiod.html).
    - start pigpiod on system start e.g. through /etc/rc.local
    - start light.py on system startup e.g. through /etc/rc.local:
      python3 
    - install Apache2 + PHP and copy the index.php into the HTTP server root
## Access and Usage
    - use button press
    - use webserver listening on local address
    - use telnet to port 12345 and write color name to trigger (e.g. red, green, blue, purple, off)
    - use Python to connect to socket listening on 12345 and send color command to there
